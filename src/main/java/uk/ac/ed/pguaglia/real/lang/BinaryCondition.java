package uk.ac.ed.pguaglia.real.lang;

import java.util.Set;

public abstract class BinaryCondition extends Condition {

	protected Condition leftCondition;
	protected Condition rightCondition;

	public BinaryCondition ( Condition left, Condition right, Condition.Type type ) {
		super(type);
		this.leftCondition = left;
		this.rightCondition = right;
	}

	@Override
	public String toString() {
		return String.format( "%s %s %s",
				leftCondition.parenthesize(),
				this.getType().getConnective(),
				rightCondition.parenthesize() );
	}

	@Override
	public Set<String> signature() {
		Set<String> sign = leftCondition.signature();
		sign.addAll(rightCondition.signature());
		return sign;
	}
}
