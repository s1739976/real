package uk.ac.ed.pguaglia.real.lang;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import uk.ac.ed.pguaglia.real.Utils;
import uk.ac.ed.pguaglia.real.db.Database;
import uk.ac.ed.pguaglia.real.db.DatabaseException;
import uk.ac.ed.pguaglia.real.db.Schema;
import uk.ac.ed.pguaglia.real.db.SchemaException;
import uk.ac.ed.pguaglia.real.db.Table;


public class Renaming extends Expression {

	private Expression operand;
	private Map<String,String> replacements;

	public Renaming (Map<String,String> replacements, Expression operand) throws ReplacementException {
		super(Expression.Type.RENAMING);
		this.operand = operand;
		if (Utils.isInjective(replacements)) {
			this.replacements = replacements;
		} else {
			throw ReplacementException.nonInjectiveMap();
		}
	}

	@Override
	public String toString() {
		Iterator<Map.Entry<String,String>> it = replacements.entrySet().iterator();
		List<String> subst = new ArrayList<String>();
		while ( it.hasNext() ) {
			Map.Entry<String,String> repl = it.next();
			subst.add( repl.getKey() + "->" + repl.getValue() );
		}
		return String.format( "%s[%s]( %s )",
				this.getType().getConnective(),
				String.join(",", subst),
				operand.toString() );
	}

	@Override
	public String toSyntaxTreeString(String prefix, Schema schema) {
		String tree = "%1$s%2$s\n%3$s |\n%3$s +--- %4$s";
		String conn = this.getType().getConnective();
		String repl = replacements.toString()
				.replace(" ", "")
				.replace("{", "[")
				.replace("}", "]")
				.replace("=", "->");
		String s = Utils.toSyntaxTreeString(operand, schema, prefix, "      ");
		return String.format(tree, conn, repl, prefix, s);
	}

	@Override
	public Set<String> signature(Schema schema) throws SchemaException {
		Set<String> attributes = Utils.clone(operand.signature(schema));
		int origSize = attributes.size();
		Set<String> keys = replacements.keySet();
		for (String k : keys) {
			if (attributes.contains(k) == false) {
				throw SchemaException.attributeNotFound(k, this, schema);
			}
		}
		Set<String> values = new HashSet<>(replacements.values());

		attributes.removeAll(keys);
		attributes.addAll(values);
		if (attributes.size() < origSize) {
			throw SchemaException.renamingDropsAttributes(this, schema);
		}
		return attributes;
	}

	@Override
	public Table execute(Database db, boolean bags) throws DatabaseException {
		signature(db.schema());
		return operand.execute(db,bags).rename(replacements);
	}

	@Override
	public Set<String> getBaseNames() {
		return operand.getBaseNames();
	}
}
