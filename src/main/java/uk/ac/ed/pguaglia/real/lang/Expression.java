package uk.ac.ed.pguaglia.real.lang;

import java.util.Set;

import org.antlr.v4.runtime.BailErrorStrategy;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import uk.ac.ed.pguaglia.real.db.Database;
import uk.ac.ed.pguaglia.real.db.DatabaseException;
import uk.ac.ed.pguaglia.real.db.Schema;
import uk.ac.ed.pguaglia.real.db.SchemaException;
import uk.ac.ed.pguaglia.real.db.Table;
import uk.ac.ed.pguaglia.real.parsing.RALexer;
import uk.ac.ed.pguaglia.real.parsing.RAParser;
import uk.ac.ed.pguaglia.real.parsing.RealListener;
import uk.ac.ed.pguaglia.real.parsing.ThrowingErrorListener;

public abstract class Expression {

	public static Expression parse(String s) throws RecognitionException, ReplacementException {
		RALexer lexer = new RALexer(CharStreams.fromString(s));
		lexer.addErrorListener(ThrowingErrorListener.INSTANCE);
		RAParser parser = new RAParser(new CommonTokenStream(lexer));
		parser.setErrorHandler(new BailErrorStrategy());
		parser.addErrorListener(ThrowingErrorListener.INSTANCE);
		try {
			ParseTree tree = parser.start();
			RealListener listener = new RealListener();
			ParseTreeWalker.DEFAULT.walk(listener, tree);
			return listener.parsedExpression();
		} catch (RuntimeException e) {
			Throwable cause = e.getCause();
			if (cause instanceof RecognitionException) {
				throw (RecognitionException) cause;
			} else if (cause instanceof ReplacementException) {
				throw new ReplacementException(e.getMessage(), cause);
			} else {
				throw e;
			}
		}
	}

	private static String getConnective(int tokenID) {
		String literal = RALexer.VOCABULARY.getLiteralName(tokenID);
		return literal.replaceAll("'", "");
	}

	public static enum Type {
		BASE         (""),
		DISTINCT     (Expression.getConnective(RALexer.DISTINCT)),
		SELECTION    (Expression.getConnective(RALexer.SELECTION)),
		PROJECTION   (Expression.getConnective(RALexer.PROJECTION)),
		RENAMING     (Expression.getConnective(RALexer.RENAMING)),
		PRODUCT      (Expression.getConnective(RALexer.PRODUCT)),
		UNION        (Expression.getConnective(RALexer.UNION)),
		DIFFERENCE   (Expression.getConnective(RALexer.DIFFERENCE)),
		INTERSECTION (Expression.getConnective(RALexer.INTERSECTION));

		private final String connective;

		private Type ( String connective ) {
			this.connective = connective;
		}

		public String getConnective() {
			return connective;
		}
	}

	private final Type type;

	public Expression (Type type) {
		this.type = type;
	}

	public Type getType() {
		return type;
	}

	public String toSyntaxTreeString() {
		return toSyntaxTreeString(null);
	}

	public String toSyntaxTreeString(Schema schema) {
		return toSyntaxTreeString("", schema);
	}

	public abstract String toSyntaxTreeString(String prefix, Schema schema);

	public abstract Set<String> signature(Schema schema) throws SchemaException;

	public abstract Table execute(Database db, boolean bags) throws DatabaseException;

	public abstract Set<String> getBaseNames();
}
