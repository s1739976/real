package uk.ac.ed.pguaglia.real.db;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import uk.ac.ed.pguaglia.real.lang.Expression;

public class Database {

	private File dataFile;
	private File schemaFile;
	private Schema schema;

	public Database() throws IOException {
		dataFile = File.createTempFile("real-", ".db");
		dataFile.deleteOnExit();
		schemaFile = File.createTempFile("real-", ".json");
		schemaFile.deleteOnExit();
		schema = new Schema();
	}

	public Database(File src) throws IOException {
		dataFile = File.createTempFile("real-", ".db");
		dataFile.deleteOnExit();
		schemaFile = src;
		schema = Schema.fromJSON(src);
	}

	public boolean save() throws IOException {
		return save(schemaFile, true);
	}

	public boolean save(File dest, boolean overwrite) throws IOException {
		if (!dest.createNewFile() && !overwrite) {
			return false;
		}
		SchemaMapper.getInstance().writeValue(dest, schema);
		this.schemaFile = dest;
		return true;
	}

	public Schema schema() {
		return schema;
	}

	public Table getView(String name, boolean bags) throws DatabaseException {
		try {
			return schema.getViewDefinition(name).execute(this, bags);
		} catch (Exception e) {
			throw new DatabaseException(e);
		}
	}

	public Table fetchTable(String name) throws DatabaseException {
		try {
			List<String[]> records = new ArrayList<>();
			File datafile = schema.getTableDatafile(name);
			Reader in = new FileReader(datafile);
			CSVParser csv = CSVFormat.RFC4180.parse(in);
			List<String> attributes = schema.getTableAttributes(name);

			for (CSVRecord r : csv.getRecords()) {
				int n = r.size();
				if (attributes.size() != n) {
					throw new DatabaseException("Record size does not match");
				}
				String[] rec = new String[n];
				for (int i=0; i < n; i++) {
					rec[i] = r.get(i);
				}
				records.add(rec);
			}
			return new Table(records, attributes);
		} catch (Exception e) {
			throw new DatabaseException(e);
		}
	}

	public Table execute(Expression expr, boolean bags) throws DatabaseException {
		return expr.execute(this, bags);
	}
}
