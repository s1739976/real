package uk.ac.ed.pguaglia.real.db;

@SuppressWarnings("serial")
public class DatabaseException extends Exception {

	public DatabaseException() {
		super();
	}

	public DatabaseException(Throwable t) {
		super(t);
	}

	public DatabaseException(String msg) {
		super(msg);
	}
}
