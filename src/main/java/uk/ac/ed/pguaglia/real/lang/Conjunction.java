package uk.ac.ed.pguaglia.real.lang;

import java.util.Map;

public class Conjunction extends BinaryCondition {

	public Conjunction ( Condition left, Condition right ) {
		super( left, right, Condition.Type.CONJUNCTION );
	}

	@Override
	public boolean satisfied(String[] record, Map<String, Integer> attr) {
		return super.leftCondition.satisfied(record, attr)
				&& super.rightCondition.satisfied(record, attr);
	}
}
