package uk.ac.ed.pguaglia.real.lang;

import java.util.Set;

import uk.ac.ed.pguaglia.real.db.Database;
import uk.ac.ed.pguaglia.real.db.DatabaseException;
import uk.ac.ed.pguaglia.real.db.Schema;
import uk.ac.ed.pguaglia.real.db.SchemaException;
import uk.ac.ed.pguaglia.real.db.Table;

public abstract class BinaryOperation extends Expression {

	protected Expression leftOperand;

	public Expression getLeftOperand() {
		return leftOperand;
	}

	public Expression getRightOperand() {
		return rightOperand;
	}

	protected Expression rightOperand;

	public BinaryOperation ( Expression left, Expression right, Expression.Type type ) {
		super(type);
		this.leftOperand = left;
		this.rightOperand = right;
	}

	@Override
	public String toString() {
		return String.format( "%s %s %s",
				parenthesize(leftOperand),
				this.getType().getConnective(),
				parenthesize(rightOperand) );
	}

	private String parenthesize( Expression expr ) {
		String str = expr.toString();
		switch ( expr.getType() ) {
		case PRODUCT:
		case UNION:
		case DIFFERENCE:
		case INTERSECTION:
			return String.format("( %s )", str);
		default:
			return str;
		}
	}

	@Override
	public String toSyntaxTreeString(String prefix, Schema schema) {
		String tree = "%1$s\n%2$s |\n%2$s +--- %3$s\n%2$s +--- %4$s";
		String conn = this.getType().getConnective();
		String s1;
		if ((leftOperand.getType() == Expression.Type.BASE)
				&& ((schema == null) || ((BaseExpression) leftOperand).isTable(schema))) {
			s1 = leftOperand.toSyntaxTreeString("", schema);
			s1 += "\n" + prefix + " |";
		} else {
			s1 = leftOperand.toSyntaxTreeString(prefix + " |    ", schema);
		}
		String s2;
		if ((rightOperand.getType() == Expression.Type.BASE)
				&& ( (schema == null) || ((BaseExpression) rightOperand).isTable(schema))) {
			s2 = rightOperand.toSyntaxTreeString("", schema);
			s2 += "\n" + prefix;
		} else {
			s2 = rightOperand.toSyntaxTreeString(prefix + "      ", schema);
		}
		return String.format(tree, conn, prefix, s1, s2);
	}

	@Override
	public Set<String> signature(Schema schema) throws SchemaException {
		Set<String> attrLeft = leftOperand.signature(schema);
		Set<String> attrRight = rightOperand.signature(schema);
		if (attrLeft.containsAll(attrRight) && attrRight.containsAll(attrLeft)) {
			return attrLeft;
		} else {
			throw SchemaException.noSameAttributes(this, schema);
		}
	}

	@Override
	public Table execute(Database db, boolean bags) throws DatabaseException {
		signature(db.schema());
		Table t1 = leftOperand.execute(db,bags);
		Table t2 = rightOperand.execute(db,bags);
		switch (getType()) {
		case INTERSECTION:
			return Table.intersectAll(t1, t2);
		case UNION:
			return bags ? Table.unionAll(t1, t2) : Table.unionAll(t1, t2).distinct();
		case DIFFERENCE:
			return t1.exceptAll(t2);
		case PRODUCT:
			return Table.product(t1, t2);
		default:
			throw new RuntimeException();
		}
	}

	@Override
	public Set<String> getBaseNames() {
		Set<String> base = leftOperand.getBaseNames();
		base.addAll(rightOperand.getBaseNames());
		return base;
	}
}
