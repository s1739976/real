package uk.ac.ed.pguaglia.real.lang;

import java.util.Set;

import uk.ac.ed.pguaglia.real.db.Database;
import uk.ac.ed.pguaglia.real.db.DatabaseException;
import uk.ac.ed.pguaglia.real.db.Schema;
import uk.ac.ed.pguaglia.real.db.SchemaException;
import uk.ac.ed.pguaglia.real.db.Table;

public class Distinct extends Expression {

	private Expression expr;

	public Distinct(Expression expr) {
		super(Type.DISTINCT);
		this.expr = expr;
	}

	@Override
	public String toSyntaxTreeString(String prefix, Schema schema) {
		String tree = "%1$s\n%2$s |\n%2$s +--- %3$s";
		String conn = this.getType().getConnective();
		String s;
		if (expr.getType() == Expression.Type.BASE) {
			s = expr.toSyntaxTreeString("", schema);
			s += "\n" + prefix;
		} else {
			s = expr.toSyntaxTreeString(prefix + "      ", schema);
		}
		return String.format(tree, conn, prefix, s);
	}

	@Override
	public Set<String> signature(Schema schema) throws SchemaException {
		return expr.signature(schema);
	}

	@Override
	public Table execute(Database db, boolean bags) throws DatabaseException {
		signature(db.schema());
		return bags ? expr.execute(db,true).distinct() : expr.execute(db,false);
	}

	@Override
	public String toString() {
		return String.format( "%s( %s )",
				this.getType().getConnective(),
				expr.toString() );
	}

	@Override
	public Set<String> getBaseNames() {
		return expr.getBaseNames();
	}
}
