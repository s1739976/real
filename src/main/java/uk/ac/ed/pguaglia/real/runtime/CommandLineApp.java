package uk.ac.ed.pguaglia.real.runtime;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.antlr.v4.runtime.RecognitionException;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.MissingOptionException;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.jline.reader.LineReader;
import org.jline.reader.LineReaderBuilder;
import org.jline.terminal.Terminal;
import org.jline.terminal.TerminalBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;

import uk.ac.ed.pguaglia.real.db.Database;
import uk.ac.ed.pguaglia.real.db.Schema;
import uk.ac.ed.pguaglia.real.db.SchemaException;
import uk.ac.ed.pguaglia.real.db.SchemaMapper;
import uk.ac.ed.pguaglia.real.db.Table;
import uk.ac.ed.pguaglia.real.lang.Expression;
import uk.ac.ed.pguaglia.real.lang.ReplacementException;

public final class CommandLineApp {

	private enum Tree { ON, OFF }
	private enum Eval { OFF, SET, BAG }

	private enum Commands {
		QUIT, SCHEMA, TREE, EVAL, DROP, SAVE, LOAD, ADD, TABLES, VIEWS;
	}

	private static final String APP_COMMAND = "java -jar <path/to/real-X.Y.Z.jar>";

	private static final String OPT_DATA_SHORT = "d";
	private static final String OPT_DATA_LONG  = "database";

	private static final String OPT_EXPR_SHORT = "q";
	private static final String OPT_EXPR_LONG  = "query";

	private static final String OPT_EVAL_SHORT = "e";
	private static final String OPT_EVAL_LONG  = "eval";

	private static final String OPT_TREE_SHORT = "t";
	private static final String OPT_TREE_LONG  = "tree";

	private static final Options setupCliOptions() {
		final Options options = new Options();
		options.addOption(Option.builder(OPT_DATA_SHORT).longOpt(OPT_DATA_LONG)
				.hasArg().argName("path/to/database/file").build());
		options.addOption(Option.builder(OPT_EXPR_SHORT).longOpt(OPT_EXPR_LONG)
				.hasArg().argName("expression").build());
		options.addOption(Option.builder(OPT_EVAL_SHORT).longOpt(OPT_EVAL_LONG)
				.hasArg().argName("semantics").build());
		options.addOption(Option.builder(OPT_TREE_SHORT).longOpt(OPT_TREE_LONG)
				.hasArg(false).build());
		return options;
	}

	public static final void main( String[] args ) {

		Options opts = setupCliOptions();
		HelpFormatter formatter = new HelpFormatter();

		CommandLineParser optParser = new DefaultParser();
		CommandLine cliCmd = null;
		try {
			cliCmd = optParser.parse( opts, args );
		} catch (MissingOptionException e2) {
			formatter.printHelp( APP_COMMAND, opts );
			System.exit(-1);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			System.exit(-1);
		}

		Property<Tree> parseTree = new Property<>(
				Commands.TREE.toString().toLowerCase(),
				Tree.class,
				Tree.OFF );

		if (cliCmd.hasOption(OPT_TREE_SHORT)) {
			parseTree.currentOption = Tree.ON;
		}

		Property<Eval> evalQuery = new Property<>(
				Commands.EVAL.toString().toLowerCase(),
				Eval.class,
				Eval.SET );

		if (cliCmd.hasOption(OPT_EVAL_SHORT)) {
			String arg = cliCmd.getOptionValue(OPT_EVAL_SHORT);
			if (evalQuery.parseOption(arg.toUpperCase()) == false) {
				System.err.println(String.format("Unrecognized value \"%s\"%nPossible values are [ %s ]", arg,
						String.join(" | ", evalQuery.optionsAsStrings())));
				System.exit(-1);
			}
		}

		final boolean batch = cliCmd.hasOption(OPT_EXPR_SHORT);

		Terminal terminal = null;
		Database db = null;
		try {
			terminal = TerminalBuilder.terminal();
			if (cliCmd.hasOption(OPT_DATA_SHORT)) {
				File src = new File(cliCmd.getOptionValue(OPT_DATA_SHORT));
				db = new Database(src);
			} else {
				db = new Database();
			}
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-1);
		}

		LineReader lineReader = LineReaderBuilder.builder()
				.terminal(terminal)
				.build();
		String prompt = "$> ";

		String viewName = null;

		mainLoop: do {
			String line = null;
			if (batch == false) {
				line = lineReader.readLine(prompt).strip();
				if (line.startsWith(".")) {
					int spc = line.indexOf(" ");
					String cmdName;
					if (spc > 0) {
						cmdName = line.substring(1,spc);
						line = line.substring(spc).strip();
					} else {
						cmdName = line.substring(1);
						line = "";
					}
					Commands cmd;
					try {
						cmd = Commands.valueOf(cmdName.toUpperCase());
					} catch (IllegalArgumentException e) {
						System.err.println(String.format("Unrecognized command \".%s\"", cmdName));
						continue mainLoop;
					}
					cmdSwitch: switch (cmd) {
					case QUIT:
						if (line.isBlank() == false) {
							System.err.println(String.format("WARNING: ignoring \"%s\"", line));
						}
						break mainLoop;
					case TABLES:
						if (line.isBlank() == false) {
							System.err.println(String.format("WARNING: ignoring \"%s\"", line));
						}
						print(Commands.TABLES, db.schema());
						continue mainLoop;
					case VIEWS:
						if (line.isBlank() == false) {
							System.err.println(String.format("WARNING: ignoring \"%s\"", line));
						}
						print(Commands.VIEWS, db.schema());
						continue mainLoop;
					case SCHEMA:
						if (line.isBlank() == false) {
							System.err.println(String.format("WARNING: ignoring \"%s\"", line));
						}
						try {
							String info = SchemaMapper.getInstance().writerWithDefaultPrettyPrinter()
									.writeValueAsString(db.schema());
							System.out.println(info);
						} catch (JsonProcessingException e) {
							e.printStackTrace();
						}
						break cmdSwitch;
					case TREE:
						if (line.isBlank()) {
							System.out.println(parseTree.status("Printing of syntax tree is "));
						} else if (parseTree.parseOption(line.toUpperCase()) == false) {
							System.err.println(String.format("Unrecognized option \"%s\"%n%s", line, parseTree.usage("USAGE: ")));
						}
						break cmdSwitch;
					case EVAL:
						if (line.isBlank()) {
							System.out.println(evalQuery.status("Query evaluation is "));
						} else if (evalQuery.parseOption(line.toUpperCase()) == false) {
							System.err.println(String.format("Unrecognized option \"%s\"%n%s", line, evalQuery.usage("USAGE: ")));
						}
						break cmdSwitch;
					case DROP:
						String name = line.strip();
						try {
							db.schema().drop(name);
							System.out.println(String.format("INFO: deleted %s", name));
						} catch (Exception e) {
							System.err.println(String.format("ERROR: %s", e.getMessage()));
							System.err.flush();
						}
						continue mainLoop;
					case LOAD:
						try {
							File src = new File(line.strip());
							db = new Database(src);
						} catch (IOException e) {
							System.out.println(String.format("ERROR: %s", e.getMessage()));
						}
						continue mainLoop;
					case SAVE:
						save(db, line, false);
						continue mainLoop;
					case ADD:
						addTable(line, db.schema());
						continue mainLoop;
					}
					continue mainLoop;
				}
			} else {
				line = cliCmd.getOptionValue(OPT_EXPR_SHORT);
			}
			if (line.isBlank() == true) {
				continue mainLoop;
			}
			if (line.contains(":=")) {
				String[] viewDef = line.split(":=");
				viewName = viewDef[0].strip();
				line = viewDef[1].strip();
			}

			Expression expr = null;
			try {
				expr = Expression.parse(line);
			} catch (RecognitionException e1) {
				System.err.println(String.format("ERROR: Cannot parse expression \"%s\".", line));
				System.err.flush();
				continue mainLoop;
			} catch (ReplacementException e2) {
				System.err.println(String.format("ERROR: In \"%s\": %s", e2.getMessage(), e2.getCause().getMessage()));
				System.err.flush();
				continue mainLoop;
			}

			if (parseTree.currentOption == Tree.ON) {
				System.out.println(expr.toSyntaxTreeString());
			}
			if (viewName != null) {
				try {
					expr.signature(db.schema());
					db.schema().addView(viewName,expr);
				} catch (SchemaException e) {
					System.err.println(String.format("ERROR: In \"%s\": %s", e.getExpression(), e.getMessage()));
					System.err.flush();
				} finally {
					viewName = null;
				}
				continue mainLoop;
			}

			if (evalQuery.currentOption == Eval.OFF) {
				try {
					expr.signature(db.schema());
				} catch (SchemaException e) {
					System.err.println(String.format("ERROR: In \"%s\": %s", e.getExpression(), e.getMessage()));
					System.err.flush();
				}
				continue mainLoop;
			}

			try {
				Table answer = db.execute(expr, evalQuery.currentOption == Eval.BAG);
				System.out.println(answer);
			} catch (SchemaException e) {
				System.err.println(String.format("ERROR: In \"%s\": %s", e.getExpression(), e.getMessage()));
				System.err.flush();
				continue mainLoop;
			} catch (IllegalStateException e) {
				System.err.println(String.format("ERROR: In \"%s\": %s", expr, e.getMessage()));
				System.err.flush();
				continue mainLoop;
			} catch (Exception e) {
				e.printStackTrace();
				System.err.flush();
				continue mainLoop;
			}
		} while (batch == false);
	}

	private static boolean save(Database db, String line, boolean overwrite) {
		try {
			if (line.isBlank()) {
				return db.save();
			}
			File f = new File(line.strip());
			return db.save(f, overwrite);
		} catch (IOException e) {
			String msg = String.format("ERROR: %s. File not saved", e.getMessage());
			System.out.println(msg);
		}
		return false;
	}

	private static void addTable(String line, Schema sch) {
		String[] lines = line.split(":");
		String filename = lines[1].strip();
		String spec = lines[0];
		int start = spec.indexOf("(");
		int end = spec.indexOf(")");
		String tableName = spec.substring(0,start).strip();
		List<String> attributes = new ArrayList<>();
		for (String attr : spec.substring(start+1,end).split(",")) {
			attributes.add(attr.strip());
		}
		try {
			sch.addTable(tableName, attributes, new File(filename));
		} catch (Exception e) {
			System.err.println("ERROR: " + e.getMessage());
		}
	}

	private static void print(Commands cmd, Schema sch) {
		Set<String> names;
		String noNamesMsg = String.format("NO %s FOUND", cmd.toString().toUpperCase());
		String fmtSuffix = "";
		switch (cmd) {
		case TABLES:
			names = sch.getTableNames();
			fmtSuffix = " : { %s }";
			break;
		case VIEWS:
			names = sch.getViewNames();
			fmtSuffix = " := %s";
			break;
		default:
			throw new IllegalStateException();
		}
		if (names.isEmpty()) {
			System.out.println(noNamesMsg);
			return;
		}
		int m = names.stream().map(String::length).max((x,y) -> x-y).get();
		String fmt = "%" + m + "s" + fmtSuffix;
		for (String name : names) {
			if (cmd == Commands.TABLES) {
				System.out.println(String.format(fmt, name, String.join(", ", sch.getTableAttributes(name))));
			}
			if (cmd == Commands.VIEWS) {
				System.out.println(String.format(fmt, name, sch.getViewDefinition(name).toString()));
			}
		}
	}
}
